#include "Application.h"



Application::Application()
{
	std::cout << "Constructing Application..." << std::endl << std::endl;	m_running = true;

	//create managers
	std::cout << "Constructed Application successfully." << std::endl << std::endl;

	//create Screen Manager
}

Application::~Application()
{
	std::cout << "Destructed Application." << std::endl << std::endl;
}





void Application::init()
{
	
	// create screens
	
	m_screenManager = std::shared_ptr<SDLScreenManager>(new SDLScreenManager());
	m_screenManager->createWindow("World Generator", 1600, 900);
	m_screenManager->init();
	



}


bool Application::run()
{

	

	while (m_running == true)
	{
		

		while (SDL_PollEvent(&m_SDLEvent))
		{
			switch (m_SDLEvent.type)
			{
			case SDL_WINDOWEVENT:
				switch (m_SDLEvent.window.event)
				{
				case SDL_WINDOWEVENT_CLOSE:
					m_running = false;
					break;
				}
				break;
			case SDL_QUIT:
				m_running = false;
				break;
			}
			
		}
		m_screenManager->update();
		m_screenManager->render();
	}
	return m_running;
}
