#pragma once
#include <iostream>
#include <SDL.h>
#include <memory>
#include "../Screen/SDLScreenManager.h"


class Application
{
public:
	Application();
	~Application();

	void init();
	bool run();

private:

	bool m_running;
	SDL_Event m_SDLEvent;
	std::shared_ptr<SDLScreenManager> m_screenManager;

	
};

