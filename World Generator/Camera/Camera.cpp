#include "Camera.h"




Camera::Camera()
{
	//eye vector
	eye.x=82.0f;
	eye.y=84.7f;
	eye.z=183.0f;

	//at vector
	at.x=82.1f;
	at.y=85.0f;
	at.z=182.0f;

	//up vector
	up.x=0.0f;
	up.y=2.0f;
	up.z=0.0f;


	m_fieldOfView = 80.0;
	m_width = 720.0;
	m_height = 640.0;
	m_aspect = m_width / m_height; // aspect ratio
	m_nearClip = 0.1;
	m_farClip = 1000.0;
	m_projectionMatrix = glm::perspective(m_fieldOfView, m_aspect, m_nearClip, m_farClip); // projection matrix
	
}

void Camera::setEye(glm::vec3 eye2)
{
	eye.x= eye2.x;
	eye.y= eye2.y;
	eye.z= eye2.z;
}



void Camera::setAt(glm::vec3 at2)
{
	at.x= at2.x;
	at.y= at2.y;
	at.z= at2.z;
}

void Camera::setUp(glm::vec3 at2)
{
	at.x= at2.x;
	at.y= at2.y;
	at.z= at2.z;
}

void Camera::setAtEye(float atEye)
{
	at.y = at.y+ atEye;

}