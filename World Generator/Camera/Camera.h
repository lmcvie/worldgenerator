#ifndef CAMERA_H
#define CAMERA_H


#include <SDL.h>
#include <glm\glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <GL/glew.h>



class Camera {
public:
	Camera();
	~Camera() { return; } 
	void setEye(glm::vec3 eye2);
	
	glm::vec3 getEye(){ return eye;}

	void setAt(glm::vec3 at2);
	
	glm::vec3 getAt(){ return at;}

	void setUp(glm::vec3 up2);
	
	glm::vec3 getUp(){ return up;}

	void setAtEye(float atEye);

	glm::mat4 getProjectionMatrix(){ return m_projectionMatrix; }
private:
	glm::vec3 eye;
	glm::vec3 at;
	glm::vec3 up;

	GLfloat m_fieldOfView;	
	GLfloat m_width; 
	GLfloat m_height; 
	GLfloat m_aspect; 
	GLfloat m_nearClip; 
	GLfloat m_farClip; 
	glm::mat4 m_projectionMatrix;
};

#endif
