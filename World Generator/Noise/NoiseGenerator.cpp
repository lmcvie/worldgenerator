#include "NoiseGenerator.h"
#include "../ppm.h"


NoiseGenerator::NoiseGenerator()
{
	

}


NoiseGenerator::~NoiseGenerator()
{


}

// 2d takes in width and height and is used to create a noisy image for testing
void NoiseGenerator::PerlinNoise2D(int height, int width, int seed)
{

	// Create an empty PPM image
	ppm image(width, height);

	clock_t start;
	double diff;
	start = clock();
	
	// Create a PerlinNoise object with the reference permutation vector
	PerlinNoise perlin2D(seed);
	m_noiseVector.resize((width*height));

	unsigned int k = 0;
	// Visit every pixel of the image and assign a color generated with Perlin noise
	for (unsigned int i = 0; i < height; ++i) {     // y
		for (unsigned int j = 0; j < width; ++j) {  // x
			double x = (double)j / ((double)width);
			double y = (double)i / ((double)height);


			// Typical Perlin noise
			double n = perlin2D.applyNoise(10 * x, 10* y, 0.1);
			m_noiseVector[k] = 100*n;

			image.r[k] = floor(255 * n);
			image.g[k] = floor(255 * n);
			image.b[k] = floor(255 * n);
			k++;
		}
	}

	diff = (std::clock() - start) / (double)CLOCKS_PER_SEC;
	std::cout << "printf: " << diff << '\n';
	// Save the image in a binary PPM file
	image.write("Perlinnoise.ppm");


}

// only difference between 3d and 2d perlin noise at this stage is that the 3d variant has a depth parameter
void NoiseGenerator::PerlinNoise3D(int height, int width, int depth, int seed)
{
	clock_t start;
	double diff;
	start = clock();
	// Create a PerlinNoise object with the reference permutation vector
	PerlinNoise perlin3D(seed);
	m_noiseVector.resize((width*height));

	unsigned int k = 0;
	// Visit every pixel of the image and assign a color generated with Perlin noise
	for (unsigned int i = 0; i < height; ++i) {     // y
		for (unsigned int j = 0; j < width; ++j) {  // x
			double x = (double)j / ((double)width);
			double y = (double)i / ((double)height);
			double z = (double)i / ((double)height);

			// Typical Perlin noise
			double n = perlin3D.applyNoise(10 * x, 10 * y, 10 * z);
			m_noiseVector[k] = 100 * n;


			k++;
		}
	}

	diff = (std::clock() - start) / (double)CLOCKS_PER_SEC;
	std::cout << "printf: " << diff << '\n';

}


void NoiseGenerator::SimplexNoise2D(int height, int width, int seed)
{
	// Create an empty PPM image
	ppm image(width, height);

	clock_t start;
	double diff;
	start = clock();
	
	// Create a PerlinNoise object with the reference permutation vector
	SimplexNoise simplex2D(seed);
	m_noiseVector.resize((width*height));

	unsigned int k = 0;
	// Visit every pixel of the image and assign a color generated with Perlin noise
	for (unsigned int i = 0; i < height; ++i) {     // y
		for (unsigned int j = 0; j < width; ++j) {  // x
			double x = (double)j / ((double)width);
			double y = (double)i / ((double)height);


			// Typical Perlin noise
			double n = simplex2D.scaledSimplex2D(0.0, 1.0, 10 * x, 10 * y);
			m_noiseVector[k] = 100 * n;

			image.r[k] = floor(255 * n);
			image.g[k] = floor(255 * n);
			image.b[k] = floor(255 * n);
			k++;
		}
	}
	diff = (std::clock() - start) / (double)CLOCKS_PER_SEC;
	std::cout << "printf: " << diff << '\n';
	// Save the image in a binary PPM file
	image.write("Simplexnoise.ppm");

}


void NoiseGenerator::SimplexNoise3D(int height, int width, int depth, int seed)
{

	clock_t start;
	double diff;
	start = clock();
	SimplexNoise simplex3D(seed);
	m_noiseVector.resize((width*height));

	unsigned int k = 0;
	// Visit every pixel of the image and assign a color generated with Perlin noise
	for ( int i = 0; i < height; ++i) 
	{     // y
		for ( int j = 0; j < width; ++j) 
		{  // x
			double x = (double)j / ((double)width);
			double y = (double)i / ((double)height);
			double z = (double)i / ((double)depth);

			// Typical Perlin noise
			double n = simplex3D.scaledSimplex3D(0.0, 1.0, 10 * x, 10 * y, 10 * z);
			m_noiseVector[k] = 100 * n;
			k++;
		}
	}
	diff = (std::clock() - start) / (double)CLOCKS_PER_SEC;
	std::cout << "printf: " << diff << '\n';
}

