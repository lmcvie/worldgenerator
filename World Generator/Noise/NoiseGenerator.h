/*Liam McVie 2014 World Generator Honours Project*/
#include <iostream>
#include <array>
#include <stdlib.h> 
#include <ctime>
#include <glm\glm.hpp>
#include <vector>
#include "../PerlinNoise.h"
#include "../SimplexNoise.h"



#pragma once

class NoiseGenerator
{

public:

	NoiseGenerator();
	~NoiseGenerator();


	void PerlinNoise2D(int worldHeight, int worldWidth, int seed);
	
	void PerlinNoise3D(int worldHeight, int worldWidth, int worldDepth, int seed); // 3d version has a depth input

	void SimplexNoise2D(int height, int width, int seed);
	void SimplexNoise3D(int height, int width, int depth, int seed);

	const std::vector<float>& getNoiseVector() { return m_noiseVector; }

private:
	std::vector<float> m_noiseVector;
	


	


};