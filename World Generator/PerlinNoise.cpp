#include "PerlinNoise.h"
#include <iostream>
#include <cmath>
#include <random>
#include <algorithm>
#include <numeric>




// Generate the permutation vector on construction using the seed as part of the engine
PerlinNoise::PerlinNoise(unsigned int seed) {

	m_pVector.resize(256);

	// Fill p with values from 0 to 255
	std::iota(m_pVector.begin(), m_pVector.end(), 0);
	
	// Initialize a random engine with seed
	std::default_random_engine engine(seed);

	// Suffle  using the above random engine
	std::shuffle(m_pVector.begin(), m_pVector.end(), engine);

	// Duplicate the permutation vector
	m_pVector.insert(m_pVector.end(), m_pVector.begin(), m_pVector.end());
}

// uses permutation vector and coordinates to create noise value
double PerlinNoise::applyNoise(double x, double y, double z) {
	// Find the unit cube that contains the point
	int X = (int) floor(x) & 255;
	int Y = (int) floor(y) & 255;
	int Z = (int) floor(z) & 255;

	// Find relative x, y,z of point in cube
	x -= floor(x);
	y -= floor(y);
	z -= floor(z);

	// Compute fade curves for each of x, y, z
	double u = fade(x);
	double v = fade(y);
	double w = fade(z);

	// Hash coordinates of the 8 cube corners
	int A = m_pVector[X] + Y;
	int AA = m_pVector[A] + Z;
	int AB = m_pVector[A + 1] + Z;
	int B = m_pVector[X + 1] + Y;
	int BA = m_pVector[B] + Z;
	int BB = m_pVector[B + 1] + Z;

	// Add blended results from 8 corners of cube
	double result = linearInterpolate(w, linearInterpolate(v, linearInterpolate(u, gradient(m_pVector[AA], x, y, z), gradient(m_pVector[BA], x - 1, y, z)),linearInterpolate(u, gradient(m_pVector[AB], x, y - 1, z), gradient(m_pVector[BB], x - 1, y - 1, z))), linearInterpolate(v, linearInterpolate(u, gradient(m_pVector[AA + 1], x, y, z - 1), gradient(m_pVector[BA + 1], x - 1, y, z - 1)), linearInterpolate(u, gradient(m_pVector[AB + 1], x, y - 1, z - 1), gradient(m_pVector[BB + 1], x - 1, y - 1, z - 1))));
	return (result + 1.0)/2.0;
}

double PerlinNoise::fade(double t) { 
	return t * t * t * (t * (t * 6 - 15) + 10);
}

double PerlinNoise::linearInterpolate(double t, double a, double b) { 
	return a + t * (b - a); 
}


double PerlinNoise::gradient(int hash, double x, double y, double z) {
	int h = hash & 15;
	
	// Convert lower 4 bits of hash into 12 gradient directions
	
	double u = h < 8 ? x : y, v = h < 4 ? y : h == 12 || h == 14 ? x : z;
	
	return ((h & 1) == 0 ? u : -u) + ((h & 2) == 0 ? v : -v);
}
