#include <vector>

#pragma once

// Code manually created and parts interpreted and taken from Ken Perlin's java implementation of improved perlin noise
// java implementation is copyright 2002

class PerlinNoise {
	
public:
	// Generate a new permutation vector based on the value of seed
	PerlinNoise(unsigned int seed);
	// Get a noise value, for 2D images z can have any value
	double applyNoise(double x, double y, double z);
private:
	double fade(double t);
	double linearInterpolate(double t, double a, double b);
	double gradient(int hash, double x, double y, double z);


	
	std::vector<int> m_pVector; // permutation vector
};

