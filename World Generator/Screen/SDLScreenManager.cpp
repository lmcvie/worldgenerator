#include "SDLScreenManager.h"


#pragma region Class Constructors & Destructors
/*
 * Constructs SDLScreenManager class.
 */
SDLScreenManager::SDLScreenManager()
{

	m_displayDevice = 0;

	std::cout << "SDLScreenManager constructed." << std::endl;

	m_screenMap["SimluationScreen"] = std::shared_ptr<SimulationScreen>(new SimulationScreen());

	m_screenMap["CurrentScreen"] = m_screenMap["SimluationScreen"];
}

/*
 * Destructs SDLScreenManager class.
 */
SDLScreenManager::~SDLScreenManager()
{
	SDL_GL_DeleteContext(m_GLContext);
	SDL_DestroyWindow(m_window);
	SDL_Quit();
	std::cout << std::endl;
	std::cout << "SDLScreenManager destructed." << std::endl;
}

#pragma endregion



void SDLScreenManager::init()
{
	m_screenMap["CurrentScreen"]->init();

	std::cout << "Screen Initialised" << std::endl;
	
}


void SDLScreenManager::update()
{
	m_screenMap["CurrentScreen"]->update();
}


void SDLScreenManager::render()
{
	m_screenMap["CurrentScreen"]->render();
	SDL_GL_SwapWindow(m_window);
}



void SDLScreenManager::setCurrentScreen(std::string window)
{
	m_screenMap["CurrentScreen"] = m_screenMap[window];
}

void SDLScreenManager::createWindow(std::string title, unsigned int width, unsigned int height)
{
    


    
	m_windowTitle = title;
	m_windowWidth = width;
	m_windowHeight = height;

	if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
	{
		std::cout << "SDL_Init: " << SDL_GetError() << std::endl;
	}

	float GLMajorVersion = 3.3;
	float GLMinorVersion = 3.3;

	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE); 
	SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);
	

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, GLMajorVersion);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, GLMinorVersion);

	SDL_Window* window;

	window = SDL_CreateWindow(m_windowTitle.c_str(), SDL_WINDOWPOS_CENTERED_DISPLAY(m_displayDevice), SDL_WINDOWPOS_CENTERED_DISPLAY(m_displayDevice), m_windowWidth, m_windowHeight, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
	
	if (window == NULL)
	{
		std::cout << "SDL_CreateWindow: " << SDL_GetError() << std::endl;
	}

	m_GLContext = SDL_GL_CreateContext(window); // Create OpenGL context and attach to window
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err)
	{
		std::cout << "glewInit failed, aborting." << std::endl;
		exit(1);
	}


	std::cout << std::endl << "Program Running Using" << std::endl;
	std::cout << "Version: " << glGetString(GL_VERSION) << std::endl;
	std::cout << "Vendor: " << glGetString(GL_VENDOR) << std::endl;
	std::cout << "Renderer: " << glGetString(GL_RENDERER) << std::endl;
	std::cout << "GLSL Version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;


	//// Get number of displays, if more than 1 then create window on display device 1 (second monitor)
	if (SDL_GetNumVideoDisplays() > 1)
	{
		m_displayDevice = 1;
	}
	else
	{
		m_displayDevice = 0;
	}

	m_window = window;
	SDL_GL_SetSwapInterval(1);	
	

	
}

