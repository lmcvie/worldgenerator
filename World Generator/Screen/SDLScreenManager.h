/*  SDL SCREEN MANAGER .h and .cpp files were taken from Martin
	Grant from a joint group project and have been heavily edited with his permission*/

#pragma once

#include <iostream>
#include <unordered_map>
#include <memory>
#include <SDL.h>
#include <GL\glew.h>
#include "ScreenManager.h"
#include "Screen.h"
#include "SimulationScreen.h"


class SDLScreenManager : public ScreenManager
{
public:
	/* Class Constructors & Destructors */
	SDLScreenManager();
	virtual ~SDLScreenManager();

public:
	/* General Public Methods */
	virtual void init();
	virtual void update();
	virtual void render();

public:
	/* Screen Management */
	virtual void setCurrentScreen(std::string window);

public:
	/* Window Management */
	virtual void createWindow(std::string title, unsigned int width, unsigned int height);

	

private:
	/* Screen Management */
	SDL_GLContext m_GLContext;
	SDL_Window* m_window;
	unsigned int m_displayDevice;

	std::string m_windowTitle;
	unsigned int m_windowWidth;
	unsigned int m_windowHeight;

	std::unordered_map<std::string, std::shared_ptr<Screen>> m_screenMap;

private:
	
};

