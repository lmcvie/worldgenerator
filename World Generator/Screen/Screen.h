/*  SCREEN .h file was taken from Martin
Grant from a joint group project and has been heavily edited with his permission*/

#pragma once



class Screen
{
public:
	/* Class Constructor & Destructor */
	virtual ~Screen() {}

public:
	/* General Public Methods */
	virtual void init() = 0;
	virtual void update() = 0;
	virtual void render() = 0;

private:
};

