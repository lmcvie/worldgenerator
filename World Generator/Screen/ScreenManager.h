/* SCREEN MANAGER .h file was taken from Martin
Grant from a joint group project and has been heavily edited with his permission*/


#pragma once
#include <string>
class ScreenManager
{
public:
	/* Virtual Constructor */
	virtual ~ScreenManager(void) {}

public:
	/* General Public Methods */
	virtual void init() = 0;
	virtual void update() = 0;
	virtual void render() = 0;

public:
	/* Screen Management */
	virtual void setCurrentScreen(std::string window) = 0;

public:
	/* Window Management */
	virtual void createWindow(std::string title, unsigned int width, unsigned int height) = 0;
	
	

};