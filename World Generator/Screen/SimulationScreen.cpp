#include "SimulationScreen.h"
#include "../ppm.h"
#include "../PerlinNoise.h"


#pragma region Class Constructors & Destructors




SimulationScreen::SimulationScreen()
{
	//manager / engine pointers
	m_textureLoader = std::shared_ptr<TextureLoader>(new TextureLoader());
	m_noiseGenerator = std::shared_ptr<NoiseGenerator>(new NoiseGenerator());
	m_camera = std::shared_ptr<Camera>(new Camera());
	m_rotate = 0.0f;
	


}


SimulationScreen::~SimulationScreen()
{
}



void SimulationScreen::init()
{
	m_polyMode = false;
	
	regenerateParams(); // ask user for parameter input
	

	//init shader and skybox
	m_shaderManager = std::shared_ptr<ShaderManager>(new ShaderManager());	
	m_skybox = std::shared_ptr<Skybox>(new Skybox("Sky"));

	rt3d::loadObj("Assets/Objects/cube.obj", m_cubeVerts, m_cubeNorms, m_cubeTex_coords, m_cubeIndices);
	GLuint cubeSize = m_cubeIndices.size();
	m_meshIndexCount = cubeSize;
	m_meshObjects[0] = rt3d::createMesh(m_cubeVerts.size() / 3, m_cubeVerts.data(), nullptr, m_cubeNorms.data(), m_cubeTex_coords.data(), cubeSize, m_cubeIndices.data());
	
	m_texture = rt3d::loadBitmap("Assets/Texture/stones.bmp");

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
}

void SimulationScreen::regenerateParams()
{
	std::cout << "Enter 1 for perlin noise or 0 for simplex noise): " << std::endl;
	std::cin >> m_noise;

	std::cout << "Enter height of world you would like ( must be a positive integer value): " << std::endl;
	std::cin >> m_height;

	std::cout << "Enter width of world you would like ( must be a positive integer value): " << std::endl;
	std::cin >> m_width;

	std::cout << "Enter depth of world you would like ( must be a positive integer value): " << std::endl;
	std::cin >> m_depth;

	std::cout << "Enter seed of world you would like ( must be a positive integer value): " << std::endl;
	std::cin >> m_seed;

	if (m_noise) {
		m_noiseGenerator->PerlinNoise3D(m_height, m_width, m_depth, m_seed);
		std::cout << "Generation of Perlin 3D Noise Complete " << std::endl;
	}

	if (!m_noise)
	{
		m_noiseGenerator->SimplexNoise3D(m_height, m_width, m_depth, m_seed);
		std::cout << "Generation of Simplex 3D Noise Complete " << std::endl;
	}

	generateWorldVector();
}

void SimulationScreen::generateWorldVector()
{

	/*m_worldVector.resize(m_width*m_height);

	int k = 0;

	for (int i = 0; i < m_height; ++i)
	{

		for (int j = 0; j < m_width; ++j)
		{
			m_worldVector[k] = j; // sets first coordinate of each 3 to 1 + increment
			m_worldVector[k + 1] = m_noiseGenerator->getNoiseVector().at(k); // sets y value to noise value
			m_worldVector[k + 2] = i; // sets z coordinate (only changes when width changes)

			m_worldVector[k + 3] = j; // sets first coordinate of each 3 to 1 + increment
			m_worldVector[k + 4] = m_noiseGenerator->getNoiseVector().at(k+1); // sets y value to noise value
			m_worldVector[k + 5] = 1 +i; // sets z coordinate (only changes when width changes)

			m_worldVector[k + 6] = 1 + j; // sets first coordinate of each 3 to 1 + increment
			m_worldVector[k + 7] = m_noiseGenerator->getNoiseVector().at(k+2); // sets y value to noise value
			m_worldVector[k + 8] = 1 + i; // sets z coordinate (only changes when width changes)

			m_worldVector[k + 9] = 1 + j; // sets first coordinate of each 3 to 1 + increment
			m_worldVector[k + 10] = m_noiseGenerator->getNoiseVector().at(k+3); // sets y value to noise value
			m_worldVector[k + 11] = i; // sets z coordinate (only changes when width changes)

			k + 12;

		}
	}
	*/
	
	m_worldVector.resize(m_width*m_height * 10);
	
	int k = 0;
	int l = 0;


	for (int i = 0; i < m_height; i++) // loop till i = width
	{
		for (int j = 0; j< m_width; j++) //loop till j = height
		{

			m_worldVector[l * 9] = 1 + j; // sets first coordinate of each 3 to 1 + increment
			m_worldVector[(l * 9) + 1] = m_noiseGenerator->getNoiseVector().at(k); // sets y value to noise value
			m_worldVector[(l * 9) + 2] = 1 + i; // sets z coordinate (only changes when width changes)

			m_worldVector[(l * 9) + 3] = 1 + j; // sets first coordinate of each 3 to 1 + increment
			m_worldVector[(l * 9) + 4] = m_noiseGenerator->getNoiseVector().at(k); // sets y value to noise value
			m_worldVector[(l * 9) + 5] = 2 + i; // sets z coordinate (only changes when width changes)

			m_worldVector[(l * 9) + 6] = 2 + j; // sets first coordinate of each 3 to 1 + increment
			m_worldVector[(l * 9) + 7] = m_noiseGenerator->getNoiseVector().at(k); // sets y value to noise value
			m_worldVector[(l * 9) + 8] = 2 + i; // sets z coordinate (only changes when width changes)

			k++;
			l++;
		}
	}

	
	m_meshObjects[1] = rt3d::createMesh(m_worldVector.size() / 3, m_worldVector.data());
}

/*
* Used to update the screen.
*/
void SimulationScreen::update()
{

	const Uint8 *keys = SDL_GetKeyboardState(NULL);
	// moving camera 

	if (keys[SDL_SCANCODE_W])
	{
		m_camera->setEye(rt3d::moveForward(m_camera->getEye(), m_rotate, 0.5f));
		m_camera->setAt(rt3d::moveForward(m_camera->getAt(), m_rotate, 0.5f));
	}
	if (keys[SDL_SCANCODE_S])
	{
		m_camera->setEye(rt3d::moveForward(m_camera->getEye(), m_rotate, -0.5f));
		m_camera->setAt(rt3d::moveForward(m_camera->getAt(), m_rotate, -0.5f));
	}
	
	if (keys[SDL_SCANCODE_A])
	{
		m_camera->setEye(rt3d::moveRight(m_camera->getEye(), m_rotate, -0.5f));
		m_camera->setAt(rt3d::moveRight(m_camera->getAt(), m_rotate, -0.5f));
	}
		
	if (keys[SDL_SCANCODE_D])
	{
		m_camera->setEye(rt3d::moveRight(m_camera->getEye(), m_rotate, 0.5f));
		m_camera->setAt(rt3d::moveRight(m_camera->getAt(), m_rotate, 0.5f));
	}
	
	if (keys[SDL_SCANCODE_R])
	{
		m_camera->setEye(glm::vec3(m_camera->getEye().x, m_camera->getEye().y + 0.5, m_camera->getEye().z));
		m_camera->setAt(glm::vec3(m_camera->getAt().x, m_camera->getAt().y + 0.5, m_camera->getAt().z));
	
	}
	if (keys[SDL_SCANCODE_F])
	{
		m_camera->setEye(glm::vec3(m_camera->getEye().x, m_camera->getEye().y - 0.5, m_camera->getEye().z));
		m_camera->setAt(glm::vec3(m_camera->getAt().x, m_camera->getAt().y - 0.5, m_camera->getAt().z));
		
	}

	if (keys[SDL_SCANCODE_O])
	{

		m_camera->setAtEye(0.1);

	}

	if (keys[SDL_SCANCODE_L])
	{
		m_camera->setAtEye(-0.1);

	}

	if (keys[SDL_SCANCODE_Z])
	{

		if (!m_polyMode)
		{
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			m_polyMode = true;
		}
		else if (m_polyMode)
		{
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			m_polyMode = false;
		}
	}
	
	if (keys[SDL_SCANCODE_1])
	{
		std::cout << "x" << std::endl;
		std::cout << m_camera->getEye().x << std::endl;
		std::cout << "y" << std::endl;
		std::cout << m_camera->getEye().y << std::endl;
		std::cout <<"z" << std::endl;
		std::cout << m_camera->getEye().z << std::endl;

	}

	if (keys[SDL_SCANCODE_2]) regenerateParams();
}



/*
* Used to render the screen.
*/

void SimulationScreen::renderWorld()
{

	glUseProgram(m_shaderManager->getShader("texture"));


	glUniformMatrix4fv(glGetUniformLocation(m_shaderManager->getShader("texture"), "projectionMatrix"), 1, GL_FALSE, glm::value_ptr(m_camera->getProjectionMatrix()));

	glm::mat4 modelview(1.0); // set base position for scene
	modelview = glm::lookAt(m_camera->getEye(), m_camera->getAt(), m_camera->getUp());
	glDepthMask(GL_TRUE);


	//identity matrix so we can access model matrix
	glActiveTexture(GL_TEXTURE0); // texture unit for prop
	glBindTexture(GL_TEXTURE_2D, m_texture);
	glUniformMatrix4fv(glGetUniformLocation(m_shaderManager->getShader("texture"), "modelViewMatrix"), 1, GL_FALSE, glm::value_ptr(modelview));
	
	rt3d::drawMesh(m_meshObjects[1], m_worldVector.size() / 3, GL_TRIANGLES);




}


void SimulationScreen::renderSkybox()
{
	
	GLuint textureID = m_skybox->getSkyboxTextureID();
	glm::mat4 m_viewMatrix = glm::lookAt(m_camera->getEye(), m_camera->getAt(), m_camera->getUp());
	glDepthMask(GL_FALSE);
	glUseProgram(m_shaderManager->getShader("skybox"));
	glUniformMatrix4fv(glGetUniformLocation(m_shaderManager->getShader("skybox"), "viewMatrix"), 1, GL_FALSE, glm::value_ptr(m_viewMatrix));
	glUniformMatrix4fv(glGetUniformLocation(m_shaderManager->getShader("skybox"), "projectionMatrix"), 1, GL_FALSE, glm::value_ptr(m_camera->getProjectionMatrix()));
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);
	glCullFace(GL_FRONT);
	rt3d::drawIndexedMesh(m_meshObjects[0], m_meshIndexCount, GL_TRIANGLES);
	glCullFace(GL_BACK);

}
void SimulationScreen::render()
{
	glDisable(GL_CULL_FACE);
	glEnable(GL_CULL_FACE);
	glClearColor(0.2f, 0.2f, 0.2f, 0.52f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//renderSkybox();
	renderWorld();


	
}
