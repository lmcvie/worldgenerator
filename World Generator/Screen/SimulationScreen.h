#pragma once

#include <iostream>
#include <memory>
#include <stack>
#include <iostream>
#include <GL\glew.h>
#include <glm\glm.hpp>
#include <glm/ext.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "../Texture/TextureLoader.h"
#include "../Noise/NoiseGenerator.h"
#include "../ShaderManager/ShaderManager.h"
#include "../Skybox/Skybox.h"
#include "../Camera/Camera.h"
#include "../rt3d.h"
#include "../rt3dObjLoader.h"






#include "../Screen/Screen.h"


class SimulationScreen : public Screen
{
public:
	/* Class Constructors & Destructors */
	SimulationScreen();
	virtual ~SimulationScreen();
	void renderSkybox();
	void renderWorld();
	void generateWorldVector();
	void regenerateParams();

public:
	/* General Public Methods */
	virtual void init();
	virtual void update();
	virtual void render();
	

private:

	std::shared_ptr<NoiseGenerator> m_noiseGenerator;
	std::shared_ptr<TextureLoader> m_textureLoader;
	std::shared_ptr<ShaderManager> m_shaderManager;

	std::shared_ptr<Skybox> m_skybox;

	std::shared_ptr<Camera> m_camera;
	GLfloat m_rotate;

	//test cube parameter members
	std::vector<GLfloat> m_cubeVerts;
	std::vector<GLfloat> m_cubeNorms;
	std::vector<GLfloat> m_cubeTex_coords;
	std::vector<GLuint> m_cubeIndices;

	GLuint m_meshIndexCount;
	GLuint m_meshObjects[2]; // array so more objects can be added
	GLuint m_texture;

	std::vector<float> m_worldVector;

	int m_height;
	int m_width;
	bool m_polyMode;
	int m_depth;
	int m_seed;
	bool m_noise;

};