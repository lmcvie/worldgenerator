#include "ShaderManager.h"


ShaderManager::ShaderManager()
{
	initNewShader("shaders/skybox.vert", "shaders/skybox.frag", "skybox");
	initNewShader("shaders/texture.vert", "shaders/texture.frag", "texture");
	glUseProgram(m_shadermap["skybox"]);
}


ShaderManager::~ShaderManager()
{
}


GLuint ShaderManager::initNewShader(const char *vert, const char *frag, std::string shaderName) {
	GLuint p;
	

	char *vs, *fs;



	GLuint v = glCreateShader(GL_VERTEX_SHADER);
	GLuint f = glCreateShader(GL_FRAGMENT_SHADER);


	// load shaders & get length of each
	GLint vlen;
	GLint flen;

	vs = loadFile(vert, vlen);
	fs = loadFile(frag, flen);


	const char* vv = vs;
	const char* ff = fs;


	glShaderSource(v, 1, &vv, &vlen);
	glShaderSource(f, 1, &ff, &flen);


	GLint compiled;

	glCompileShader(v);
	glGetShaderiv(v, GL_COMPILE_STATUS, &compiled);
	if (!compiled) {
		std::cout << "Vertex shader not compiled." << std::endl;
		printShaderError(v);
		return false;
	}

	glCompileShader(f);
	glGetShaderiv(f, GL_COMPILE_STATUS, &compiled);
	if (!compiled) {
		std::cout << "Fragment shader not compiled." << std::endl;
		printShaderError(f);
		return false;
	}

	p = glCreateProgram();

	glAttachShader(p, v);
	glAttachShader(p, f);

	glBindAttribLocation(p, 0, "VS_position");
	glBindAttribLocation(p, 1, "VS_normal");
	glBindAttribLocation(p, 2, "VS_texCoord");


	glLinkProgram(p);

	glDetachShader(p, v);
	glDetachShader(p, f);

	glUseProgram(p);

	delete[] vs;
	delete[] fs;

	m_shadermap[shaderName] = p;

	return true;
}



char* ShaderManager::loadFile(const char *fname, GLint &fSize)
{
	int size;
	char * memblock;

	// file read based on example in cplusplus.com tutorial
	// ios::ate opens file at the end
	std::ifstream file(fname, std::ios::in | std::ios::binary | std::ios::ate);
	if (file.is_open()) {
		size = (int)file.tellg(); // get location of file pointer i.e. file size
		fSize = (GLint)size;
		memblock = new char[size];
		file.seekg(0, std::ios::beg);
		file.read(memblock, size);
		file.close();
		std::cout << "file " << fname << " loaded" << std::endl;
	}
	else {
		std::cout << "Unable to open file " << fname << std::endl;
		fSize = 0;
		// should ideally set a flag or use exception handling
		// so that calling function can decide what to do now
		return nullptr;
	}
	return memblock;
}


// from rt3d.h by Daniel Livingstone
void ShaderManager::printShaderError(const GLint shader)
{
	int maxLength = 0;
	int logLength = 0;
	GLchar *logMessage;

	// Find out how long the error message is
	if (!glIsShader(shader))
		glGetProgramiv(shader, GL_INFO_LOG_LENGTH, &maxLength);
	else
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);

	if (maxLength > 0) { // If message has some contents
		logMessage = new GLchar[maxLength];
		if (!glIsShader(shader))
			glGetProgramInfoLog(shader, maxLength, &logLength, logMessage);
		else
			glGetShaderInfoLog(shader, maxLength, &logLength, logMessage);
		std::cout << "Shader Info Log:" << std::endl << logMessage << std::endl;
		delete[] logMessage;
	}
}


GLuint ShaderManager::getShader(std::string shaderName)
{
	
	if (m_shadermap.count(shaderName) == 1) // code taken from andrew devlin from joint project
	{
		return m_shadermap[shaderName];
	}
	else
	{
		std::cout << shaderName.c_str() << " not found in map." << std::endl;
		return NULL;
	}
}
