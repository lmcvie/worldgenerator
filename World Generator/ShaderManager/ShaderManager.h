#pragma once

#include <string>
#include <fstream>
#include <iostream>
#include <unordered_map>
#include <memory>
#include <GL/glew.h>

class ShaderManager
{
public:
	ShaderManager();
	~ShaderManager();
	GLuint initNewShader(const char *vert, const char *frag, std::string shaderName);
	void ShaderManager::printShaderError(const GLint shader);
	char* ShaderManager::loadFile(const char *fname, GLint &fSize);
	GLuint ShaderManager::getShader(std::string shaderName);

private:
	std::unordered_map<std::string, GLuint> m_shadermap;

};

