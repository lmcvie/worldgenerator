#version 330
precision highp float;

layout (location=0) in vec3 in_position;
in vec3 in_texCoord;
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
 
out vec3 ex_texCoord;

void main() 
{

	vec4 vertexPosition = viewMatrix * vec4(in_position,1.0);
    gl_Position = projectionMatrix * vertexPosition;

	ex_texCoord = in_texCoord;
}