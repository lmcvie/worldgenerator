// textured.frag
#version 330

// Some drivers require the following
precision highp float;



in vec2 ex_TexCoord;
in vec4 out_Position;
layout(location = 0) out vec4 out_Color;
 
void main(void) {
    
	// Fragment colour

//	if (out_Position.y  >  0.0f )  
//	{
//	out_Color = vec4(1.0f,1.0f,1.0f,1.0f);
//	}

	out_Color = vec4(0.0f,0.5f,0.0f,1.0f);
}