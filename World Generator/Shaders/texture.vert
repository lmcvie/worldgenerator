
#version 330

uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;


in  vec3 in_Position;

out vec4 out_Position;
// multiply each vertex position by the MVP matrix
void main(void) {

	
	// vertex into eye coordinates
	vec4 vertexPosition = modelViewMatrix * vec4(in_Position,1.0);
    gl_Position = projectionMatrix * vertexPosition;

	vec4 out_Position = gl_Position;
}