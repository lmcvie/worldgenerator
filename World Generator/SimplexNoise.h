#pragma once
#include <vector>
#include <iostream>
#include <cmath>
#include <random>
#include <algorithm>
#include <numeric>

// influence and aid was taken from Gustavson, Stefan (2005) Simplex noise demystified. 
// whilst implementing and parts of code have been taken from his implementation as well but have been edited as well as changed to suit different methods

class SimplexNoise
{
public:
	/* Class Constructors & Destructors */
	~SimplexNoise();
	SimplexNoise(unsigned int seed);

	// carries out a simplex algorithm then scales the results before returning the noise value
	float scaledSimplex2D(const float loBound, const float hiBound, const float x, const float y);
	float scaledSimplex3D(const float loBound, const float hiBound, const float x, const float y, const float z);
	

private:

	
	// Raw Simplex noise - unscaled
	float simplex2D(const float x, const float y);
	float simplex3D(const float x, const float y, const float z);

	
	int fastfloor(const float x); // function to floor a value quickly and efficiently

	// dot product functions for 2D and 3D
	float dot2D(const int* g, const float x, const float y);
	float dot3D(const int* g, const float x, const float y, const float z);
	

	std::vector<int> m_pVector; // permutation vector
	
};