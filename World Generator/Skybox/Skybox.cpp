#include "Skybox.h"



Skybox::Skybox(std::string textureName)
{
	loadSkybox(textureName);

}


Skybox::~Skybox()
{
}

void Skybox::loadSkybox(std::string textureName)
{
	std::string fname = "assets/Skybox/" + textureName;

	m_fileName[0] = fname + "-right.bmp";
	m_fileName[1] = fname + "-left.bmp";
	m_fileName[2] = fname + "-up.bmp";
	m_fileName[3] = fname + "-down.bmp";
	m_fileName[4] = fname + "-front.bmp";
	m_fileName[5] = fname + "-back.bmp";

	

	glGenTextures(1, &m_skyboxTextureID);
	glBindTexture(GL_TEXTURE_CUBE_MAP, m_skyboxTextureID);

	SDL_Surface* xPositive = SDL_LoadBMP(m_fileName[0].c_str()); //right
	
	SDL_Surface* xNegative = SDL_LoadBMP(m_fileName[1].c_str()); //left
	
	SDL_Surface* yPositive = SDL_LoadBMP(m_fileName[2].c_str()); //top

	SDL_Surface* yNegative = SDL_LoadBMP(m_fileName[3].c_str()); //bottom

	SDL_Surface* zPositive = SDL_LoadBMP(m_fileName[4].c_str()); //back
	
	SDL_Surface* zNegative = SDL_LoadBMP(m_fileName[5].c_str()); //front
	

	SDL_PixelFormat *format = xPositive->format;

	GLuint externalFormat, internalFormat;
	if (format->Amask) {
		internalFormat = GL_RGBA;
		externalFormat = (format->Rmask < format->Bmask) ? GL_RGBA : GL_BGRA;
	}
	else {
		internalFormat = GL_RGB;
		externalFormat = (format->Rmask < format->Bmask) ? GL_RGB : GL_BGR;
	}

	glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, internalFormat, xPositive->w, xPositive->h, 0, externalFormat, GL_UNSIGNED_BYTE, xPositive->pixels);
	glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, internalFormat, xNegative->w, xNegative->h, 0, externalFormat, GL_UNSIGNED_BYTE, xNegative->pixels);
	glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, internalFormat, yPositive->w, yPositive->h, 0, externalFormat, GL_UNSIGNED_BYTE, yPositive->pixels);
	glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, internalFormat, yNegative->w, yNegative->h, 0, externalFormat, GL_UNSIGNED_BYTE, yNegative->pixels);
	glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, internalFormat, zPositive->w, zPositive->h, 0, externalFormat, GL_UNSIGNED_BYTE, zPositive->pixels);
	glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, internalFormat, zNegative->w, zNegative->h, 0, externalFormat, GL_UNSIGNED_BYTE, zNegative->pixels);

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	SDL_FreeSurface(xPositive);
	SDL_FreeSurface(xNegative);
	SDL_FreeSurface(yPositive);
	SDL_FreeSurface(yNegative);
	SDL_FreeSurface(zPositive);
	SDL_FreeSurface(zNegative);
	

}

GLuint Skybox::getSkyboxTextureID()
{
	return m_skyboxTextureID;
}
