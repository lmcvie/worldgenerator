#pragma once


#include <iostream>
#include <memory>
#include <glm\glm.hpp>
#include <GL\glew.h>
#include <SDL.h>
class Skybox
{
public:
	Skybox(std::string textureName);
	~Skybox();
	void loadSkybox(std::string filename);
	void renderSkybox();
	GLuint Skybox::getSkyboxTextureID();
	GLfloat getVAO(){ return m_vao; }
private:

	
	
	std::string m_fileName[6];
	GLuint m_skyboxTextureID;
	GLuint m_vao;
	GLuint m_vbo;

};