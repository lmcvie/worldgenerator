#pragma once

#include "Application\Application.h"

int main(int argc, char *args[])
{
	Application WorldGenerator;
    
	WorldGenerator.init();
    
	while (WorldGenerator.run())
		continue;
    
	return 0;
}