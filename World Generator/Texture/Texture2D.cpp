#include "Texture2D.h"

/*
@param GLenum targetType - type of texture it is, has to be valid with glTexImage2D, GL_CUBE_MAP_POSITIONX, GL_TEXTURE_2D etc.
@param GLuint width - The width you want the texture to be
@param GLuint height - The height you want the texture to be
@param GLenum internalFormat - type that the textures data is stored as internally in OpenGL, RGBA32f etc.
@param GLenum externalFormat - type of data that the passed in data to the texture will be again RGBA etc.can use different combinations of internal and format to get data compression
@param GLenum dataType - type of data that the values are GL_FLOAT etc.
@param GLvoid* data - the data you intend to pass into the texture, can also be NULL in the case you may want to use it as a render target
*/
Texture2D::Texture2D(GLenum targetType, GLuint width, GLuint height, GLenum internalFormat, GLenum externalFormat, GLenum dataType, GLvoid* data)
: m_bindTarget(targetType)
{
	glGenTextures(1, &m_textureID);
	glBindTexture(m_bindTarget, m_textureID);
	glTexImage2D(targetType, 0, internalFormat, width, height, 0, externalFormat, dataType, data);
	glBindTexture(m_bindTarget, 0);
}

// deletes the texture from the GPU
Texture2D::~Texture2D()
{
	glDeleteTextures(1, &m_textureID);
}

// Generates mip maps for the Texture, may not work with all texture types worth checking OpenGLs
// documentation if using a type your unfamiliar with.
void Texture2D::generateMipmap()
{
	glGenerateMipmap(m_bindTarget);
}

// Depending on GL version texParameteri will only work with GL_TEXTURE_2D or GL_TEXTURE_CUBE_MAP (4.0+ works with more)

// updates how the texture handles minification
// @param GLenum filterType - type of filtering it uses when minifying(many texels into one pixel I.E pixel bigger than a single texel) - GL_NEAREST, GL_LINEAR.
void Texture2D::updateMinFilter(GLenum filterType)
{
	glTexParameteri(m_bindTarget, GL_TEXTURE_MIN_FILTER, filterType);
}

// updates how the texture handles magnification 
// @param GLenum filterType - type of filtering it uses when magnifying(1 texel spread across several pixels, I.E pixel smaller than a single texel) - GL_NEAREST, GL_LINEAR.
void Texture2D::updateMagFilter(GLenum filterType)
{
	glTexParameteri(m_bindTarget, GL_TEXTURE_MAG_FILTER, filterType);
}

// updates how the texture wraps on the S-ord
// @param GLenum wrapType - how a texture wraps around an object when a co - ordinate outside of the 0 - 1 range is given, I.E CLAMP_TO_EDGE the texture clamps to the objects edges, GL_REPEAT its repeated several times over the object
void Texture2D::updateSWrap(GLenum wrapType)
{
	glTexParameteri(m_bindTarget, GL_TEXTURE_WRAP_S, wrapType);
}

// updates how the texture wraps on the T-ord
// @param GLenum wrapType - how a texture wraps around an object when a co - ordinate outside of the 0 - 1 range is given, I.E CLAMP_TO_EDGE the texture clamps to the objects edges, GL_REPEAT its repeated several times over the object
void Texture2D::updateTWrap(GLenum wrapType)
{
	glTexParameteri(m_bindTarget, GL_TEXTURE_WRAP_T, wrapType);
}

// unbind the texture id
void Texture2D::unbindTexture()
{
	glBindTexture(m_bindTarget, 0);
}

// bind the texture id
// @param GLenum activeTexture - a texture unit value such as GL_TEXTURE0 to bind the texture to!
void Texture2D::bindTexture(GLenum activeTexture)
{
	glActiveTexture(activeTexture);
	glBindTexture(m_bindTarget, m_textureID);
}